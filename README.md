# Mutual Donation quantitative analysis

Date: Jun 2021

Author: Florian Habermacher, [florian.habermacher@gmail.com](florian.habermacher@gmail.com)

### What is this repository for? ###

* Python (PyCharm) project for quantitative exploration of Mutual Donation: Exploring a centralized voluntary donation scheme for co-incentivizing donors to contribute to a public good. This would serve as a sort of reversal of the crowding out phenomenon where, with many participants, each potential donor has a disincentive to contribute because his contribution tends to make others' donations less likely.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Python & Virtualenv: setup & use
Python: Make sure your system has Python 3.6 installed: Follow [this tutorial](https://www.pytorials.com/python-download-install-windows/) to install it if not yet installed.

For the rest, use the command line.

Virtualenv setup & use/activation:

- Globally on OS: Install virtualenv
    - `pip install virtualenv`
- Within your folder: Setup a local python copy aka virtual environment in \env\ (or \venv\ in which case replace accordingly below)
    - Setup local virtualenv in subfolder venv or env; in the following we presume env is used
        - `cd your_project\py`
        - `virtualenv env`
    - Install python requirements in your virtualenv: Change into your folder, Activate venv, and Install requirements:
        - `cd your_project\py`
        - `env\Scripts\activate.bat`
        - `pip install -r requirements.txt`
    - Option 1: Use virtualenv with Activation: activate, run python program, deactivate.
        - `cd your_project\py`
        - `env\Scripts\activate.bat` Note, you'll see "(venv)" at the beginning of the command prompt as long as venv is activated
        - `python mypythonscript.py`
        - `deactivate`
    - Option 2: Directly run your python script by simply explicitly indicating the venv intepreter, instead of 'activating' the latter:
        - `cd your_project\py`
        - `env\Scripts\python mypythonscript.py`


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact