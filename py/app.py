# Mutual Donation exploration
# Florian Habermacher
# 20210602
#

from flib import *
from ftriv import *
import pandas as pd
from pandas.api.types import is_numeric_dtype
import statsmodels
import statsmodels.api as sm
#from pytictoc import TicToc
import argparse
from scipy import stats, interpolate
from pandas.api.types import is_numeric_dtype
#import psycopg2
from operator import itemgetter
from typing import Callable, TypeVar, Iterable, Tuple, Union, List, Dict
import traceback
from threading import Lock
from collections import defaultdict
import copy
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
# # To treat all warnings as kind of errors: make them crash & also catchable using 'try' & 'except RuntimeWarning:' :
# #   From https://stackoverflow.com/a/30368735/3673329
# import warnings
# warnings.filterwarnings("error")

def ls_n0_and_check(ls, n0=None):
    """ Verify validity of leverage schedule ls
    :arg ls Leverage schedule for n=1, 2, ... Note, must be filled with 0 up to the starting of non-zero donations
    :arg n0 None or First donor number for which non-zero donation expected. Note, ls for n0 must equal n0: ls[n0-1]=n0
    :return n0 found (as float; useful as need to be careful to not use as index; use instead n0idx=int(n0-1))
    """
    N = len(ls)
    fassert.g(N,1,"Empty or single-value L schedule")
    fassert.ge(np.min(ls),0,"L schedule cannot have negative values")
    fassert.g(np.max(ls),0,"Expect some strictly positive values in L schedule")
    starting, started = False, False
    n0_found = None
    for idx in range(N):
        i = idx+1.0
        if not started:
            starting = ls[idx]>0
            if starting:
                fassert.eq(ls[idx],i,"leverage_n0 <> n0")
                if n0 is not None:
                    fassert.eq(i,n0,"Leverage schedule not starting at n0")
                n0_found = i
        if started: # Not yet if just starting
            fassert.ge(ls[idx], ls[idx - 1], "Weakly increasing leverage please!")
            #fassert.g(ls[idx],ls[idx-1],"Strictly increasing leverage please!")
            fassert.l(ls[idx],i,"Leverage l(i) [=here ls[idx]] strictly < i, please (else donation diverges)!")
        elif starting:
            started = True
            starting = False
    fassert.f(starting+started,"Logical error in L sched verification"
                               "(I thought I captured case w/o missing positive values above?)")
    fassert.notnone(n0_found,"Logical error in L sched verification: no proper n0_found (considered impossible here) ")
    return n0_found


def d_schedule(ls, d_n0 = 1):
    """(Unitary) Donation as function of Leverage factor, for 1, 2, ..., unitary donors
    :arg d_n0 donation level for first non-zero donation
    :arg ls schedule for 1st, 2nd, ... unitary donor
    """
    n0 = ls_n0_and_check(ls)
    N = len(ls)
    d = np.zeros(N)
    d[int(n0-1)] = d_n0
    for idx in range(int(n0), N):
        i = idx+1
        d[idx] = d[idx-1] * (i-1)/(i-ls[idx])
    return d

def ds_theor(n0l0,e,n,d_n0=1):
    return d_n0 * \
           (math.factorial(n-1)/(math.factorial(n0l0-1)*math.factorial(n-n0l0)) * math.exp((n-n0l0)*e) if n>n0l0 else 1)

def EDsingle(a0,a1):
    """Expected donation to poor with single rich person in
    D:\Dropbox\Writing\MutualDonation\generalmutualdonation.lyx
    Note, np.log is ln """
    def aux(a): return a-np.log(a+1)
    EDp = aux(a1)-aux(a0)
    return EDp
def EUsingle(a0,a1):
    """Expected utility of poor with single rich person in
    D:\Dropbox\Writing\MutualDonation\generalmutualdonation.lyx
    Note, np.log is ln """
    def integratelog(x): return x*np.log(x)-x
    def aux(a): return a*np.log(10) + integratelog(a)-integratelog(1+a)
    EUp = aux(a1)-aux(a0)
    return EUp

def analyse_no_scheme():
    # Analysis basic for "D:\Dropbox\Writing\MutualDonation\generalmutualdonation.lyx"
    a0,a1=0.01,0.1
    N = 100

    # - Single rich
    print(EDsingle(a0, a1))
    print(EUsingle(a0, a1))

    # - N rich
    don = np.zeros(N) # Donations
    alpha = np.random.uniform(a0,a1,N)

    maxit = 100  # Hopefully converges by then?
    it = 0
    converged = False
    don_save = np.zeros((N,maxit)) # Store iterations' donations
    # MIND, THIS IS BULLSHIT: must of course not take “conditional” contributions; instead must take into account that others give less if I give more! In fact, this elasticity is almost 1:1! So stifling contributions extremely strongly! –> must calculate typical donation reactivity by others too! Hm, bit more annoying... But can do iteratively.
    while it<maxit and not converged:
        for i in range(N):
            D_others = np.sum(don)-don[i] # Sum of all others' donations
            D_optimal = 10*alpha[i]/(1+alpha[i]) # Optimal D for donor i, if she has to pay marginal contribution
            don[i] = max(0,D_optimal - D_others)
        D = np.sum(don)
        print(f'D @ it {it} = {D}'.format(it=it,D=D))
        don_save[:,it]=don
        it+=1
    print("Mind, this was only for only 1 single alpha-draw")

def isiterable(obj):
    try:
        iter(obj)
    except TypeError:
        return False
    else:
        return True

def simpletheor():
    #Illustrate few donation schedules for simplest leverage schedules with l0=n0=1 and linear leverage increase
    def dl(l,d0=1):
        try:
            return d0*np.exp(l-1)  # Can work for scalar and np.array
        except:
            if isiterable(l):
                return np.array([dl(li,d0) for li in l])
            else:
                ferr(f'Hm, cannot do for l {l} and d0 {d0}')
                return None
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle(f'Donation Schedules d(L): Examples capping contributes at $512')
    for mode in [{'ax': ax1, 'scal':'Logarithmic'}, {'ax': ax2, 'scal':'Linear'}, ]:
        ax, scal = mode['ax'], mode['scal']
        ls=[[np.linspace(1,2,100),1,'b'],[np.linspace(1,5,100),3,'g'],[np.linspace(1,8,100),5,'r'],]
        targetdollars = 512
        legend=[]
        for i,l_ in enumerate(ls):
            l,lmin,col= l_[0],l_[1],l_[2]
            style = col+('-' if lmin==1 else '--')
            cutstr = f', left cut-off at L_min={lmin}' if lmin>1 else ''
            nam = f'L 1-{int(l[-1])}{cutstr}'
            legend.append(nam)
            ax.plot(l, dl(l) * targetdollars/dl(l[-1]),style, label=nam if lmin==1 else None)
            if lmin>1:
                ax.plot(l[l>=lmin], dl(l[l>=lmin]) * targetdollars / dl(l[-1]), col+'-', label=nam)

        if scal == 'Logarithmic':
            ax.set_yscale('log')
            ax.set_yticks([1, 8, 64, 512])
            ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
            ax.set(xlabel='Leverage L', ylabel='(Unitary) Donation d(L) [$/person]')
            ax.legend(loc='lower right')
        if scal == 'Linear':
            ax.set(xlabel='Leverage L')
        ax.set_xticks([1, 2, 3, 4, 5, 6, 7, 8])
        ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

        # We need to draw the canvas, otherwise the labels won't be positioned and
        # won't have values yet.
        fig.canvas.draw()

        labels = [item.get_text() for item in ax.get_xticklabels()]
        labels[0] = 'L=1\nn=1'
        labels[-1] = f'L={labels[-1]}\nn e.g. 1k, 1bn, ...'
        ax.set_xticklabels(labels)
    fig.savefig('D:\Dropbox\Writing\MutualDonation\media\donationschedulestheor.png')


if __name__ == '__main__':
    simpletheor()

    mode = ['Scheme','Scheme_3D','Scheme2','Scheme3','Scheme4','No_scheme'][0]
    print(mode)

    if mode=='Scheme':
        es = np.array([0.2, 0.1, 0.01, 0.001, 0.0001])
        addon = False # Whether extend all schemes with 10-times more granular scheme in the end

        ls_all = {} #{'1z': ls_1z, '1': ls_1, '1b': ls_1b, '1c': ls_1c}
        ds_all = {}
        for e in es: # for k,ls in ls_all.items():
            ls = np.arange(1,5 if addon else 10,e)
            if addon:
                e2=e/10
                ls = np.concatenate([ls,np.arange(ls[-1]+e2,10,e2)])
            ls_n0_and_check(ls)
            N = len(ls)
            nam = f'n0=1, epsi={e} (N={N})'
            ls_all[nam], ds_all[nam] = ls, d_schedule(ls)

        titbase = mode+(", Addon" if addon else "")
        plt.figure()
        for k in ls_all:
            plt.semilogy(ls_all[k], ds_all[k])
        plt.legend(ls_all.keys())
        plt.title(titbase+": L vs. d(L)")
        plt.xlabel('Leverage l')
        plt.ylabel('(Unitary) Donation d(L)')
        outfn = '../out/SchemeEpsi'+("addon" if addon else "")+'.png'
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)


    elif mode=='Scheme_3D':
        addon = False
        # Mind, "theor" versions are VERY slow! Idle them if you want (skipTheor) !
        skipTheor = True
        pars = [
            {'e': .003, 'n0': 1, 'theor':False},  {'e': .003, 'n0': 1, 'theor':True},
            {'e': .001, 'n0': 1, 'theor':False},  {'e': .001, 'n0': 1, 'theor':True},
            {'e': .0003, 'n0': 1, 'theor':False},  {'e': .0003, 'n0': 1, 'theor':True},
            # {'e': .0001, 'n0': 1, 'theor':False},  {'e': .0001, 'n0': 1, 'theor':True},
            {'e': .003, 'n0': 2, 'theor':False},  {'e': .003, 'n0': 2, 'theor':True},
            {'e': .001, 'n0': 2, 'theor':False},  {'e': .001, 'n0': 2, 'theor':True},
            {'e': .0003, 'n0': 2, 'theor':False},  {'e': .0003, 'n0': 2, 'theor':True},
            # {'e': .0001, 'n0': 2, 'theor':False},  {'e': .0001, 'n0': 2, 'theor':True},
            {'e': .003, 'n0': 4, 'theor':False},  {'e': .003, 'n0': 4, 'theor':True},
            {'e': .001, 'n0': 4, 'theor':False},  {'e': .001, 'n0': 4, 'theor':True},
            {'e': .0003, 'n0': 4, 'theor':False},  {'e': .0003, 'n0': 4, 'theor':True},
            # {'e': .0001, 'n0': 4, 'theor':False},  {'e': .0001, 'n0': 4, 'theor':True},
                ]
        def parnam(par): return f'e {par["e"]} n0 {par["n0"]} {"t" if not par["theor"] else "nott"}'
        def parcol(par):
            ncolmap={1: 'b', 2: 'g', 4: 'r', 'other': 'k'}
            estyle={.003: '-', .001: '--', .0003: '-.', .0001: ':'}
            return ncolmap[par['n0']]+estyle[par['e']] + ("x" if par['theor'] else "")
        # es = np.array([0.01, 0.003, 0.001, 0.0003, 0.0001])
        ls_all = {} #{'1z': ls_1z, '1': ls_1, '1b': ls_1b, '1c': ls_1c}
        ds_all = {}
        # for e in es: # for k,ls in ls_all.items():

        for par in pars: # for k,ls in ls_all.items():
            e, n0, theor = par['e'], par['n0'], par['theor']
            if skipTheor and theor: continue
            print(par)
            lsOrig = np.arange(1, 10, e)
            ls = lsOrig.copy()
            if n0 != 1:
                ls = ls + (n0-ls[n0-1])
                ls[:n0-1] = 0
                fassert.eq(ls[n0-1],n0,"Euhm ls starts wrongly A")
                fassert.eq(ls[n0-2],0,"Euhm ls starts wrongly B")
            ls_n0_and_check(ls)
            N = len(ls)
            nam = f'{parnam(par)} (N={N})'
            # nam = f'n0=1, epsi={e} (N={N})'
            ls_all[nam] = ls
            if par['theor']:
                ds_all[nam] = np.array([ds_theor(n0,e,n) for n in range(len(ls))])
                # ds_all[nam] = np.zeros(len(ls))
                # for n in range(len(ls)):
                #     ds_all[nam] = ds_theor(n0,e,n)
            else:
                ds_all[nam] = d_schedule(ls)

            # ls_all[nam], ds_all[nam] = ls, d_schedule(ls)

        titbase = mode+(", Addon" if addon else "")
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.zaxis._set_scale('log')
        nams = []
        # Sadly, 3D matplotlib has an unresolved log-axis issue; we have a simple workaround
        # Thanks to richinex on https://github.com/matplotlib/matplotlib/issues/209 , using it below:
        for i,k in enumerate(ls_all):
            par = pars[i]
            ls = ls_all[k]
            x = ls
            y = np.arange(len(ds_all[k]))+1.0
            z = ds_all[k]
            x[ls == 0] = np.nan
            y[ls == 0] = np.nan
            z[ls == 0] = np.nan
            style = parcol(par)
            ax.plot3D(x, y, np.log10(z), style)
            # ax.plot3D(x, y, z, style)
            nams.append(parnam(par))
        import matplotlib.ticker as mticker
        # ax.plot_surface(x, y, np.log10(z))
        def log_tick_formatter(val, pos=None):
            return "{:.2e}".format(10 ** val)
        ax.zaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))

        plt.show()
        plt.legend(nams)
        plt.title(titbase+": n vs. d(n)")
        plt.xlabel('Leverage L')
        plt.ylabel('(Unitary) Donors n')
        ax.set_zlabel('(Unitary) Donation d(n)')
        outfn = '../out/ddSchemeEpsi_3D_'+("addon" if addon else "")+'.png'
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)

        plt.figure()
        for k in ls_all:
            plt.semilogy(ls_all[k], ds_all[k])
        plt.legend(ls_all.keys())
        plt.title(titbase+": L vs. d(L)")
        plt.xlabel('Leverage l')
        plt.ylabel('(Unitary) Donation d(L)')
        outfn = '../out/SchemeEpsi'+("addon" if addon else "")+'.png'
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)




    elif mode=='Scheme2':
        ls = np.concatenate(([0,2],np.arange(2.1,10,0.1)))
        ls_n0_and_check(ls)
        ds = d_schedule(ls)
        plt.figure()
        plt.semilogy(ls, ds)
        plt.xlabel('L')
        plt.ylabel('d(L)')

    elif mode == 'Scheme3':
        # Plot two schemes: constant l-step, and l kept kept after half-way through
        es = np.array([0.2, 0.1, 0.01, 0.001, 0.0001])
        e = es[2]
        ls_all = {} #{'1z': ls_1z, '1': ls_1, '1b': ls_1b, '1c': ls_1c}
        ds_all = {}
        for i,a in enumerate(['standard','curbed']):
            ls = np.arange(1, 10, e)
            if a=='curbed':
                ls[int(len(ls)/2):]=ls[int(len(ls)/2)]
            ls_n0_and_check(ls)
            N = len(ls)
            nam = f'n0=1, epsi={e} (N={N}), {a}'
            ls_all[nam], ds_all[nam] = ls, d_schedule(ls)

        titbase = mode
        plt.figure()
        for k in ls_all:
            plt.semilogy(ls_all[k], ds_all[k])
        plt.legend(ls_all.keys())
        plt.title(titbase+": L vs. d(L)")
        plt.xlabel('Leverage l')
        plt.ylabel('(Unitary) Donation d(L)')
        outfnpart = 'SchemeEpsi'+mode+'.png'
        outfn = '../out/'+outfnpart
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)

        plt.figure()
        for k in ls_all:
            plt.semilogy(ds_all[k])
        plt.legend(ds_all.keys())
        plt.title(titbase+": n vs. d(n)")
        plt.xlabel('(Unitary) Donors n')
        plt.ylabel('(Unitary) Donation d(n)')
        outfn = '../out/dd'+outfnpart
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)


    elif mode == 'Scheme4':
        # Plot multiple schemes: constant l-step, but with different pairs n0=l0
        # ls_end_remains_const = True
        es = np.array([0.2, 0.1, 0.01, 0.001, 0.0001])
        e = es[2]
        n0l0s = np.array([1,2,3,4,5]) # In the simplest, most strict version, start has n_ = l_ always
        ls_all = {} #{'1z': ls_1z, '1': ls_1, '1b': ls_1b, '1c': ls_1c}
        ds_all = {}
        for i,n0l0 in enumerate(n0l0s):
            lsOrig = np.arange(1, 10, e)
            ls = lsOrig.copy()
            if not (n0l0==1):
                ls = ls + (n0l0-ls[n0l0-1])
                # if ls_end_remains_const:
                #     # Make sure by end, ls remains always same
                #     subtractEnd = ls[-1]-lsOrig[-1]
                #     subtractN0 = 0
                #     for n in range(n0l0,len(lsOrig)):
                #         ls[n] = ls[n] - (n-n0l0) * (subtractEnd-subtractN0)/(len(ls)-n0l0)
                ls[:n0l0-1] = 0
                fassert.eq(ls[n0l0-1],n0l0,"Euhm ls starts wrongly A")
                fassert.eq(ls[n0l0-2],0,"Euhm ls starts wrongly B")
            ls_n0_and_check(ls)
            N = len(ls)
            nam = f'n0=1, epsi={e} (N={N}), n0l0 {n0l0}'
            ls_all[nam], ds_all[nam] = ls, d_schedule(ls)

        titbase = mode
        plt.figure()
        for k in ls_all:
            plt.semilogy(ls_all[k], ds_all[k])
        plt.legend(ls_all.keys())
        plt.title(titbase+": L vs. d(L)")
        plt.xlabel('Leverage l')
        plt.ylabel('(Unitary) Donation d(L)')
        outfnpart = 'SchemeEpsi'+mode+'.png'
        outfn = '../out/'+outfnpart
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)

        plt.figure()
        for k in ls_all:
            plt.semilogy(ds_all[k])
        plt.legend(ds_all.keys())
        plt.title(titbase+": n vs. d(n)")
        plt.xlabel('(Unitary) Donors n')
        plt.ylabel('(Unitary) Donation d(n)')
        outfn = '../out/dd'+outfnpart
        mkdirIfNotExist(outfn)
        plt.savefig(outfn)

    elif mode=='No_scheme':
        analyse_no_scheme()

